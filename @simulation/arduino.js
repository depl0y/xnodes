var mdns = require("mdns");
var fs = require("fs");
var http = require("http");

var browser = mdns.createBrowser(mdns.tcp('http'));

browser.on('serviceUp', function(service) {
	if (service.name == "xnodes") {
		console.log("Found xnodes node: " + service.host + " : " + service.port);
		connectToXnodes(service.host, service.port);
		browser.stop();
	}
});
browser.on('serviceDown', function(service) {
});

browser.start();

var connectToXnodes = function(host, port) {
	host = "127.0.0.1"; // Force local host in @simulation
	console.log("Connecting to xnodes host: " + host + ":" + port);
	
	var nodeid = -1;
	
	if (fs.existsSync(__dirname + "/nodeid")) {
		nodeid = fs.readFileSync(__dirname + "/nodeid");
		// Node id exists, should send raspberry an update with my current IP address
		console.log("I already have a node id : " + nodeid);
		var url = "/nodes/register/" + nodeid + "?apikey=abcdefghijklmop";
		console.log(url);
		var options = {
			host: host,
			port: port,
			path: "/nodes/register/" + nodeid + "?apikey=abcdefghijklmop"
		};		

		http.get(options, function(resp){
			resp.on("data", function(data) {
				console.log(data.toString("utf-8"));
				
				startSimulation(nodeid, host, port);		
			});
			resp.on("error", function(e) {
				console.log(e);
			});
		});			
	}
	else {
		console.log("I do not have a node id yet, should fetch it");
		
		var options = {
			host: host,
			port: port,
			path: "/nodes/register?apikey=abcdefghijklmop"
		};
		
		http.get(options, function(resp){
			resp.on("data", function(data) {
			
				var nodeid = data.toString("utf-8");
				nodeid = "1";
			
				fs.writeFile(__dirname + "/nodeid", nodeid, function(err) {
				    if(err) {
				        console.log(err);
				    } else {
				        console.log("Node id was saved to file");
				        
				        startSimulation(nodeid, host, port);	
				    }
				}); 				
			});
			resp.on("error", function(e) {
				console.log(e);
			});
		});	
	}
}

var startSimulation = function(nodeid, host, port) {
	
	var t = function(c, node, host, port) {
		
		setInterval(function() {
			reportTemperature(node, host, port);
		}, 1100);
		
	}
	
	t(this, nodeid, host, port);
}

var reportTemperature = function(nodeid, host, port) {
	var temp = Math.floor((Math.random()*10)+1) + 15;
	
	var options = {
		host: host,
		port: port,
		path: "/nodes/store/" + nodeid + "/temp/" + temp + "?apikey=abcdefghijklmop"
	};
	
	http.get(options, function(resp){
		resp.on("data", function(data) {
			console.log(data.toString("utf-8"));
		});
		resp.on("error", function(e) {
			console.log(e);
		});
	});		
}