#include <SoftwareSerial.h>
#include <NewRemoteTransmitter.h>
#include <NewRemoteReceiver.h>

const int temperaturePin = 0; // LM35
const int digitalTemperaturePin = 3; // DS18B20

const int receiverInterupt = 0;
const int tranmitterPin = 11;

const int p1RequestPin = 4; // P1 comm port
const int p1ReceivePin = 9;

const int tempSendInterval = 1000; // in ms
const int totalLoopTime = 10; // in ms

String p1inputString;

int loopCount = 0;


SoftwareSerial softSerial(p1ReceivePin, 8, true);

void setup() {
  // put your setup code here, to run once:

  p1inputString = "";

  // Put voltage on the p1RequestPin
  pinMode(p1RequestPin, OUTPUT);
  digitalWrite(p1RequestPin, HIGH);
  
  //NewRemoteReceiver::init(receiverInterupt, 2, writeRFcode);  

  softSerial.begin(9600);

  Serial.begin(9600);
  Serial.flush();
  loopCount = 0;

}

void loop() {
  if ((loopCount * totalLoopTime) % tempSendInterval == 0) {
    float tempFloat = readAnalogTemperature();
    Serial.print("TEMP:");
    Serial.println(tempFloat);
  }

  readP1();
  readRFcommand();

  delay(10);
  
  loopCount++; 
  if (loopCount > 200)
    loopCount = 1;
}


