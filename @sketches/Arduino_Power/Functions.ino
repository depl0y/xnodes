long stringToLong(String value) {
  value = value + '\0';
  char test_as_char[value.length()];
  value.toCharArray(test_as_char, value.length());
  return atol(test_as_char);
}

int stringToInt(String value) {
  value = value + '\0';
  char test_as_char[value.length()];
  value.toCharArray(test_as_char, value.length());
  return atoi(test_as_char);  
}

String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }

  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void writeRFcode(NewRemoteCode receivedCode) {
  digitalWrite(10, HIGH);  
  Serial.print("RF:");
  Serial.print(receivedCode.address);
  Serial.print(":");
  Serial.print(receivedCode.groupBit);
  Serial.print(":");
  Serial.print(receivedCode.unit);
  Serial.print(":");
  Serial.print(receivedCode.switchType);
  Serial.print(":");
  Serial.print(receivedCode.dimLevel);
  Serial.print(":");
  Serial.print(receivedCode.period);
  Serial.println("");
  
  delay(200);
  digitalWrite(10, LOW);  
}

void readP1() {
  while (softSerial.available() > 0) {
    int incomingByte = softSerial.read();
    incomingByte &= ~(1 << 7);
    char inChar = (char) incomingByte;
    p1inputString += inChar;
  }
  
  if (p1inputString.length() > 100) {
    Serial.println("P1:START");
    Serial.println(p1inputString);
    Serial.println("P1:END");
    p1inputString = "0";
  }    
}

float readAnalogTemperature() {
  
  unsigned int totalTemp = 0;
  unsigned int temp = analogRead(temperaturePin);
  delay(60);    
    
  for (int i = 0; i <= 3; i++) {
    temp = analogRead(temperaturePin);
    totalTemp += temp;
  }

  totalTemp = totalTemp >> 2;
  float tempFloat = (float)totalTemp * 0.49;
  
  return tempFloat;  
}

void readRFcommand() {
  String readString = "";
  
  while (Serial.available()) {
    delay(3);   
    char c = Serial.read();
    readString += c;
  } 
  
  if (readString.length() > 0) {
    
    if (readString[readString.length() -1] == '\n')
      readString = readString.substring(0, readString.length() - 1);

    if (getValue(readString, ':', 0) == "RF") {
      String address = getValue(readString, ':', 1);
      String type = getValue(readString, ':', 2);
      String num = getValue(readString, ':', 3);
      String command = getValue(readString, ':', 4);
      
      bool dim = false;
      
      int dimLevel = 0;
      
      bool cmd = false;
      if (command.equals("on"))
        cmd = true;
      else if (command.equals("off"))
        cmd = false;
      else {
        dimLevel = stringToInt(command);
        dim = true;
      }
      
      NewRemoteTransmitter transmitter(stringToLong(address), tranmitterPin, 260, 3);

      if (type.equals("group")) {
        transmitter.sendGroup(cmd);
        Serial.println("group");
      }
      else {
        if (dim) {
          transmitter.sendDim(stringToLong(num), dimLevel);
        Serial.println("dim");
        }
        else {
          transmitter.sendUnit(stringToLong(num), cmd);
        }
      }
    }    
    
    readString = "";
  }
}
