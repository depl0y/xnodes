#include <mthread.h>
#include <EEPROM.h>
#include <NewRemoteTransmitter.h>
#include <NewRemoteReceiver.h>

bool writeNodeIDToEEPROM(int nodeID) {
  
  if (nodeID > 1016)
    return false;
    
  float parts = nodeID / 254;
  
  for (int i = 0; i < (int)parts; i++) {
    EEPROM.write(i, 254);
  }
  
  int left = nodeID % 254;
  EEPROM.write((int)parts, left);
  
  for (int i = parts + 1; i < 4; i++) {
    EEPROM.write(i, 0);
  }
  
  return true;
}

String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }

  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}

long stringToLong(String value) {
  value = value + '\0';
  char test_as_char[value.length()];
  value.toCharArray(test_as_char, value.length());
  return atol(test_as_char);
}

int stringToInt(String value) {
  value = value + '\0';
  char test_as_char[value.length()];
  value.toCharArray(test_as_char, value.length());
  return atoi(test_as_char);  
}

class SendRfThread : public Thread {
  public:
    SendRfThread(int pinNumber);
  
  protected:
    bool loop();
    
  private:
    int pin;
};

SendRfThread::SendRfThread(int pinNumber) {
  this->pin = pinNumber;
}

bool SendRfThread::loop() {
  String readString = "";
  
  while (Serial.available()) {
    delay(3);  //delay to allow buffer to fill 

    if (Serial.available() >0) {
      char c = Serial.read();  //gets one byte from serial buffer
      readString += c; //makes the string readString
    } 
  }

  // RF:6644474:unit:1:on
  if (readString.length() >0) {
    Serial.println(readString);
    
    if (readString[readString.length() -1] == '\n') {
      readString = readString.substring(0, readString.length() - 1);
    }
    
    if (getValue(readString, ':', 0) == "RF") {
      String address = getValue(readString, ':', 1);
      String type = getValue(readString, ':', 2);
      String num = getValue(readString, ':', 3);
      String command = getValue(readString, ':', 4);
      Serial.println(num);
      
      bool dim = false;
      
      int dimLevel = 0;
      
      bool cmd = false;
      if (command.equals("on"))
        cmd = true;
      else if (command.equals("off"))
        cmd = false;
      else {
        dimLevel = stringToInt(command);
        dim = true;
      }
      
      NewRemoteTransmitter transmitter(stringToLong(address), this->pin, 260, 3);

      if (type.equals("group")) {
        transmitter.sendGroup(cmd);
      }
      else {
        if (dim) {
          transmitter.sendDim(stringToLong(num), dimLevel);
        }
        else {
          transmitter.sendUnit(stringToLong(num), cmd);
        }
      }
    }
    
    readString = "";
  }
}

class TempThread : 
public Thread {

public:
  TempThread(int pinNumber);

protected:
  bool loop();

private:
  int pinNr;
  void LM35Temp();
};

TempThread::TempThread(int pinNumber) {
  this->pinNr = pinNumber;
}

bool TempThread::loop() {

  if (kill_flag)
    return false;

  this->LM35Temp();
  delay(1000);
  
  return true;
}

void TempThread::LM35Temp() {
  unsigned int tempread = 0;

  unsigned int intmp = analogRead(this->pinNr),
  delay(60);

  digitalWrite(13, HIGH);

  for (int i=0;i<=3;i++) {
    intmp = analogRead(this->pinNr);
    tempread = tempread + intmp;
    //    delay(200);
  }
  tempread = tempread >> 2;
  sleep(0.2);
  digitalWrite(13, LOW);
  
  float temp = (float) tempread * 0.49;
  Serial.print("TEMP:");
  Serial.println(temp);
}

void WriteCode(NewRemoteCode receivedCode) {
  digitalWrite(9, HIGH);  
  Serial.print("RF:");
  Serial.print(receivedCode.address);
  Serial.print(":");
  Serial.print(receivedCode.groupBit);
  Serial.print(":");
  Serial.print(receivedCode.unit);
  Serial.print(":");
  Serial.print(receivedCode.switchType);
  Serial.print(":");
  Serial.print(receivedCode.dimLevel);
  Serial.print(":");
  Serial.print(receivedCode.period);
  Serial.println("");
  
  delay(200);
  digitalWrite(9, LOW);  
}

//Analog pin number for the temperature sensor
int temperaturePin = 0;

//Interupt 0 = digital pin 2
int receiverInterupt = 0;

// RF transmitter pin (digital)
int transmitterPin = 11;

void setup() {
  Serial.begin(115200);
  Serial.flush();

  pinMode(13, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);

  main_thread_list->add_thread(new TempThread(temperaturePin));
  main_thread_list->add_thread(new SendRfThread(transmitterPin));
  NewRemoteReceiver::init(receiverInterupt, 2, WriteCode);  
}




