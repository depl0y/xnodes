var utils = require("./utilities/utils");
var nodeManager = require("./NodeManager");
var linq = require('linq');
var fs = require("fs");
var util = require("util");

var NodeManager = function NodeManager() {
	
	var nodes = [];
	var nodeHooks = [];
	var webServer = null;
	
	loadNode = function(configRow, context, callback) {
		
		var nodeType = configRow.value.type;
		var nodePath = utils.config.nodesPath() + nodeType + "/" + nodeType + ".js";
		
		if (!fs.existsSync(nodePath)) {
			utils.logging.info(context.identity().identifier, util.format("Node of type [%s] does not exist in nodes directory (%s)", configRow.value.type, nodePath));
		
			callback(false);
			return;
		}
		
		/* When loading a node, we first need to check if we got that node type in our "nodes" directory. */
		isNodeLoaded(configRow, function(isLoaded) {
			if (isLoaded) {
				utils.logging.verbose(context.identity().identifier, util.format("Reloading settings for %s", configRow.value.type));	
			}
			else {
				utils.logging.verbose(context.identity().identifier, util.format("Loading configuration data for %s", configRow.value.type));	
				var loadedNode = require(nodePath);
				var node = new loadedNode();				
				node.initialize(configRow, function() {
					/* Should attach to web server */
					var routes = node.webRoutes();
					for (var index in routes) {
						var routeInfo = routes[index];
						webServer.addHandler(node.identity().identifier, routeInfo.method, routeInfo.path);
					}
					
					utils.logging.verbose(context.identity().identifier, util.format("Node %s exposes %d public events", node.identity().identifier, node.publicEvents().length));
					
//					node.eventEmitter.on("data", dataReceived);
					context.nodes.push(node);
					
					callback(node);
				});
			}
		});
		
	};
	
	dataReceived = function(node, data) {
//		console.log(node.config.nodeID.toString() + " : " + node.identity().identifier + " : " + JSON.stringify(data));
	}
	
	isNodeLoaded = function(configRow, callback) {
		callback(false);
	}

	reloadRequest = function(req, res) {
		utils.logging.verbose(this.identifier, "Received request to reload nodes");
		
		for (var i = 0; i < 5; i++) {
			res.send("OK " + i);
		}
		
	}
	
	/*
	The reloadNodes method, queries the database and loads node configuration data.
	*/
	this.reloadNodes = function(callback) {
		if (nodes.length > 0)
			utils.logging.verbose(this.identity().identifier, "Reloading nodes");
		else {
			utils.logging.verbose(this.identity().identifier, "Loading nodes");
			this.nodes = [];
		}
	
		var t = function(c) {
			utils.config.getNodes(function(data, err) {
				if (!err) {
					utils.logging.verbose(c.identity().identifier, util.format("Loaded configuration for %d nodes", data.length));
					
					var returnedNodes = 0;
					for (var i in data) {
						loadNode(data[i], c, function(result) {
							returnedNodes++;
							if (result) {
								utils.logging.info(c.identity().identifier, util.format("Node %s.%d initialized succesfully", result.identity().identifier, result.config.nodeID));
							}
							else {
								
							}

							if (returnedNodes == data.length) {
								c.performHooks(c, callback);
							}
						});
					}
					
				}
			});	
		};
		
		t(this);
	};
	
	this.performHooks = function(c, callback) {
		utils.logging.info(c.identity().identifier, "Prepare to hook nodes");
	
		for (var i in c.nodes) {
			var node = c.nodes[i];
			var hooks = node.hooksInto();
			
			utils.logging.info(c.identity().identifier, util.format("Node %s.%d hooks into %d other node(s)", node.identity().identifier, node.config.nodeID, hooks.length));

			for (var hookIndex in hooks) {
				var hookNode = c.findNodeInNodes(c, hooks[hookIndex]);
				if (!hookNode)				
					utils.logging.info(c.identity().identifier, "Unable to find node to hook in");
				else {
					utils.logging.info(c.identity().identifier, util.format("Hooking %s.%d into %s.%d", node.identity().identifier, node.config.nodeID, hookNode.identity().identifier, hookNode.config.nodeID));
				}
			}
		}
	
		callback();
	}
	
	this.findNodeInNodes = function(c, hook) {
		for (var index in c.nodes) {
			if (c.nodes[index].identity().identifier == hook.node && c.nodes[index].config.nodeID == hook.nodeID)
				return c.nodes[index];
		}
		
		return false;
	}
	
	this.setWebServer = function(server) {
		webServer = server;
		webServer.addHandler(this.identity().identifier, utils.bind(reloadRequest), "/manager/reload");			
	}

	this.identity = function() {
		return {
			identifier: "NodeManager"
		};
	}
	
	if(NodeManager.caller != NodeManager.getInstance){
		throw new Error("NodeManager is a Singleton, do no instanciate");
	}	
	

}

NodeManager.instance = null;

/**
 * Singleton getInstance definition
 * @return singleton class
 */
NodeManager.getInstance = function(){
	if(this.instance === null){
		this.instance = new NodeManager();
	}
	return this.instance;
}

module.exports = NodeManager.getInstance();