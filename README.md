##xnodes

**Visit our [website](http://xnodes.org) for more information.**

-

In short: **xnodes** is a NodeJS application, which allows you to control your house.

The system is built using modules, where each module can be configured in an extensive way.

Currently we are in the process of defining the architecture and build proof of concepts, to make sure this system can use low-cost hardware to control your devices.

Of course the possibilities are endless, but just to keep ourselves under control, we try to keep it limited at first and expand from there (maybe with your help?).

###Planned features:

- Modular built system, easy to expand with new features
- Monitoring of different systems:
	- Energy meter (P1 and flashing LED)
	- Gas meter
	- Water meter
	- Router
- Remote-control of different systems:
	- RF lightning, blinds, etc
	- Thermostat
		- OpenTherm
		- Regular switch thermostat
		

###Proof of concepts
Currently we got proof of concepts up and running for the following components:

- Energy meter using the 1000W LED
- Energy meter using P1 connection
- Gas meter monitoring
- Simple thermostat functionality, using 2 wires relay function
- RF control of lights

###Hardware
We are currently using Arduino and Raspberry Pi's to test our project. The devices are really energy sufficient and have great customisability. The software can actually be installed on any device support the dependencies we have.

###Open source
We use this repo to dump our code in, to share our progress and get some comments about how to do stuff better or different. Feel free to contact us.

###Installation
Install xnodes on your own Raspberry, using [this](http://xnodes.org/developers/documentation/installing-xnodes/) guide.

