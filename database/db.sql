CREATE TABLE logging (datetime INTEGER, logtype INTEGER, module TEXT, message TEXT);
CREATE TABLE config (module TEXT, key TEXT, value TEXT);

INSERT INTO config VALUES ('service', 'port', '9000');
INSERT INTO config VALUES ("service", "host", "0.0.0.0");
INSERT INTO config VALUES ("logging", "min_level", "0");

INSERT INTO config VALUES ('nodes', 'node', '{ "type": "ApiNode", "enabled": true, "friendlyName": "Xnodes Web API", "settings": { } }');
INSERT INTO config VALUES ('nodes', 'node', '{ "type": "ArduinoNode", "enabled": true, "friendlyName": "Arduino Temp Reader", "settings": { "hoardFile": "randomData.hoard", "interval": 10, "maxValue": 200, "minValue": 0 } }');
INSERT INTO config VALUES ('nodes', 'node', '{ "type": "LoggingNode", "enabled": true, "friendlyName": "Logging Node", "settings": { "pagesize": 20 } }');
INSERT INTO config VALUES ('nodes', 'node', '{ "type": "LoadNode", "enabled": true, "friendlyName": "System load Node", "settings": { } }');