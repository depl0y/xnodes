var fs = require('fs');
var utils = require('../utilities/utils');
var async = require('async');
var init = require("./initialize");

exports.database = function(databasePath, callback) {

	fs.exists(databasePath, function(exists) {
		if (exists) {
            console.log("SQLite database exists, keeping that one...");
			init.checkDatabase(databasePath, callback);
		}
		else {
            console.log("SQLite database does not exist, creating...");
			init.createDatabase(databasePath, utils.config.databaseImportFile(), callback);
		}
	});

};

exports.createDatabase = function(databasePath, databaseFile, callback) {
    fs.readFile(databaseFile, 'utf-8', function(err, data) {
        if (err) {
        	callback(err);
        }
        else {
	        var db = utils.db.getConnection();
	
            db.exec(data, function(err) {
                if (err) {
                    callback(err);
                }
                else {
                	utils.logging.info(__filename, "Imported default values in database... done");
                    callback();
                }
            });		
        }
    });		
};

exports.checkDatabase = function(databasePath, callback) {
	console.log("Should check database structure");	
	callback();
};
