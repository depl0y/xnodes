//var http = require('http');
var utils = require('./utilities/utils');
var util = require("util");
var Server = require("./webserver/server");
var init = require("./database/initialize");
var config = require("./utilities/config");
var NodeManager = require("./NodeManager")

var async = require('async');

//console.log("\033[2J\033[0f");

global.loglevel = 0;
process.argv.forEach(function (val, index, array) {
    if (val.indexOf("log-level") > -1 && val.indexOf(":") > -1) {
        var loglevel = val.substr(val.indexOf(":") + 1);
        global.loglevel = loglevel;
        
        console.log(util.format("New log-level set to %d", global.loglevel));
    }
});

process.on('uncaughtException', function (err) {
  console.error(util.format('Caught exception: ', err));
});

console.log("Initializing database....");
init.database(config.databasePath(), function(err) {
	if (!err) {
	    utils.logging.info(__filename, "Initialization of database complete");
	    
	    initWebserver(function(err) {
		    initNodeManager(function(err) {
		    	utils.logging.info(__filename, "NodeManager started");
		    });
	    });
	}
	else {
		console.log(err);
	}
});

var initWebserver = function(callback) {

	global.server = new Server({ test: 3000 });
	global.server.start(function() {
		utils.logging.info(__filename, "Webserver started");
		NodeManager.setWebServer(global.server);
		callback();
	}, function(err) {
		utils.logging.error(__filename, util.format("Webserver start failed: %s", err));
		callback(err);
	});	
}

var initNodeManager = function(callback) {
	NodeManager.reloadNodes(function() {
		callback();
	});
}