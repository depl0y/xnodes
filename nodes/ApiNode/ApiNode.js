var NodeBase = require("../Base/NodeBase");
var util = require("util");
var events = require('events');
var Enumerable = require('linq');
var fs = require("fs");
var utils = require("../../utilities/utils");
var sp = require("serialport");
var SerialPort = require("serialport").SerialPort


var ApiNode = function() {
	NodeBase.call(this);
	this.settings = this.defaultSettings();
}

util.inherits(ApiNode, NodeBase);

var p = ApiNode.prototype;

p.defaultSettings = function() {

	return {
	
		dataTypes: ["temp", 
					"power.current.usage", 
					"power.current.delivered", 
					"power.t1.usage", 
					"power.t2.usage", 
					"power.t1.delivered", 
					"power.t2.delivered",
					"cpuload"
				   ],
	
	/*
		storage: [
			{
				name: "RRD Gauge",
				identifier: "rrdgauge"
				rrd: {
					steps: 1,
					settings: ["DS:value:GAUGE:600:U:U", "RRA:AVERAGE:0.0:1:3600", "RRA:AVERAGE:0.0:60:1440", "RRA:AVERAGE:0.0:900:2880", "RRA:AVERAGE:0.0:3600:9480"]
				}
			},
			{
				name: "RRD Counter",
				identifier: "rrdcounter"
				rrd: {
					steps: 1,
					settings: ["DS:value:COUNTER:600:U:U", "RRA:AVERAGE:0.0:1:3600", "RRA:AVERAGE:0.0:60:1440", "RRA:AVERAGE:0.0:900:2880", "RRA:AVERAGE:0.0:3600:9480"]
				}
			},
			{
				name: "SQL storage",
				identifier: "sqlstorage"
				sql: {
					
				}
			}
		],
		valueDefinitions: [
			{
				storage: "rrdgauge",
				type: "temp"
			},
			{
				storage: "rrdgauge",
				type: "power.current.usage"
			},
			{
				storage: "rrdgauge",
				type: "power.current.delivered"
			},
			{
				storage: "rrdgauge",
				type: "cpuload"
			}
		],*/
		rrd: [	
			{ 
				type: "string",
				datatype: ["temp"],
				steps: 1,
				value : ["DS:value:GAUGE:600:U:U", "RRA:AVERAGE:0.0:1:3600", "RRA:AVERAGE:0.0:60:1440", "RRA:AVERAGE:0.0:900:2880", "RRA:AVERAGE:0.0:3600:9480"]
			},
			{
				type: "string", 
				datatype: ["power.current.usage", "power.current.delivered"],
				steps: 1,
				value : ["DS:value:GAUGE:600:U:U", "RRA:AVERAGE:0.0:1:3600", "RRA:AVERAGE:0.0:60:1440", "RRA:AVERAGE:0.0:900:2880", "RRA:AVERAGE:0.0:3600:9480"]
			},
			{
				type: "string", 
				datatype: ["power.t1.usage", "power.t2.usage", "power.t1.delivered", "power.t2.delivered"],
				steps: 1,
				value : ["DS:value:COUNTER:600:U:U", "RRA:AVERAGE:0.0:1:3600", "RRA:AVERAGE:0.0:60:1440", "RRA:AVERAGE:0.0:900:2880", "RRA:AVERAGE:0.0:3600:9480"]
			},			
			{
				type: "string",
				datatype: ["cpuload"],
				steps: 1,
				value : ["DS:value:GAUGE:600:U:U", "RRA:AVERAGE:0.0:1:3600", "RRA:AVERAGE:0.0:60:1440", "RRA:AVERAGE:0.0:900:2880", "RRA:AVERAGE:0.0:3600:9480"]
			}
		]
	};
}

p.identity = function() {
	return {
		identifier : "org.xnodes.ApiNode", // required property
		version : "1.0", // required property
	};
}

p.initialize = function(configuration, callback) {
	this.config = configuration;

	this.rrdFile = util.format("%s%d.rrd", this.storagePath(), this.config.nodeID);

	/* Node needs it's own table in the database, to store RF signals */
	var connection = utils.db.getConnection();

	var createRFinfo = "CREATE TABLE rfinfo (nodeid INTEGER, friendlyname TEXT, address TEXT, isgroup INTEGER, unit INTEGER, lastreceived INTEGER);";
	connection.run(createRFinfo);

	var createNodeStore = "CREATE TABLE nodes (ipaddress TEXT, friendlyname TEXt, registered INTEGER);";
	connection.run(createNodeStore);

	callback();
}

p.webRoutes = function() {

	this.registerNode = utils.bind(this.registerNode, this);
	this.storeValue = utils.bind(this.storeValue, this);
	this.listNodes = utils.bind(this.listNodes, this);
	this.readValues = utils.bind(this.readValues, this);
	this.readLastValue = utils.bind(this.readLastValue, this);
	this.typesAvailable = utils.bind(this.typesAvailable, this);

	this.listRF = utils.bind(this.listRF, this);
	this.storeRF = utils.bind(this.storeRF, this);

	return [
		{ path: "/nodes/register", method: this.registerNode },
		{ path: "/nodes/register/:node", method: this.registerNode },
		{ path: "/nodes/list", method: this.listNodes },
		{ path: "/nodes/types/:node", method: this.typesAvailable },
		{ path: "/nodes/remove/:node", method: this.removeNode },
		{ path: "/nodes/store/:node/:type/:value", method: this.storeValue },
		{ path: "/nodes/read/:node/:type/:resolution/:startdate/:enddate", method: this.readValues },
		{ path: "/nodes/read/:node/:type/:resolution/last", method: this.readLastValue },
		{ path: "/rf/list", method: this.listRF },
		{ path: "/rf/store/:node", method: this.storeRF }
	];
}

p.publicEvents = function() {
	return [
		"temp-received",
		"rf-received"
	];
}

p.hooksInto = function() {
	return [
	];
}

p.cleanUp = function() {
	utils.logging.info(this.identity().identifier, "Cleaning up ArduinoNode node");
}

p.storeValue = function(req, res) {

	var t = function(c, request, response) {
		response.send("OK");
		c.createRRD(c, request.params.node, request.params.type, function(path, err) {
			
			if (err) {
				utils.logging.error(c.identity().identifier, err);
			}
			else {
				var now = Math.ceil((new Date).getTime() / 1000);
			
				utils.rrd.update(path, "value", [[now, request.params.value].join(":")], function(err) {
		            if (err) {
		                utils.logging.error(c.identity().identifier, util.format("%s (%s)", path, err));
		            }
		            else {
		                utils.logging.verbose(c.identity().identifier, util.format("Received %s value: %s > %s", request.params.type, request.params.value, path));
		            }
				});				
			}
		});
	};
	
	t(this, req, res);
}

p.readValues = function(req, res) {
	
	var t = function(c, request, response) {
		var resolution = request.params.resolution;
		var node = request.params.node;
		var type = request.params.type;
		var endDate = Math.ceil((new Date).getTime() / 1000);
		
		utils.logging.verbose(c.identity().identifier, util.format("[%s] Received API READ request: %s - %s - %s", request.ip, resolution, node, type));
		
		var dbPath = c.storagePath() + node.toString() + "-" + type + ".rrd";
		
		if (!fs.existsSync(dbPath)) {
			response.send("ERROR: Database for this node/type combination does not exist");
			return;
		}
		
		if (request.params.enddate != 0) {
			endDate = request.params.enddate;
		}
		
		var d = new Date();
		d.setHours(d.getHours() - 24);
		var startDate = Math.ceil(d.getTime() / 1000);
		
		if (request.params.startdate != 0) {
			startDate = request.params.startdate;
		}
		
		var v = new utils.Validator();
		v.check(resolution).isInt();
		v.check(node).isInt();
		v.check(startDate).isDecimal();
		v.check(endDate).isDecimal();
	
		var errors = v.getErrors();
		if (errors.length > 0) {
			res.end(JSON.stringify(errors));
			return;
		}

		if (startDate >= endDate) {
			response.end("Startdate can not be higher than end date, you suck");
			return;
		}
		
		c.readValuesWithDates(dbPath, startDate, endDate, resolution, function(results) {
		    res.statusCode = 200;			
			res.jsonp(results);
		});
	};
	
	t(this, req, res);	
}

p.readLastValue = function(req, res) {
	
	var t = function(c, request, response) {
		var resolution = request.params.resolution;
		var node = request.params.node;
		var type = request.params.type;

		var v = new utils.Validator();
		v.check(resolution).isInt();
		v.check(node).isInt();
		
		var errors = v.getErrors();
		if (errors.length > 0) {
			res.end(JSON.stringify(errors));
			return;
		}
		
		utils.logging.verbose(c.identity().identifier, util.format("[%s] Received API LAST request: %s - %s - %s", request.ip, resolution, node, type));

		var dbPath = c.storagePath() + node.toString() + "-" + type + ".rrd";

		var endDate = Math.ceil((new Date().getTime()) / 1000);
		
		var records = (resolution * 10);
		if (resolution == 1) {
			records = 60;
		}
		
		var startDate = endDate - records;
		
		c.readValuesWithDates(dbPath, startDate, endDate, resolution, function(results) {
			for (var index = results.length - 1; index >= 0; index--) {
				
				var o = results[index];
				if (!o || !o.value) {
					continue;
				}
				
				response.statusCode = 200;
				response.jsonp(o);
				return;
			}
			
			response.statusCode = 500;
			response.send("No record found");
		});


	};
	
	
	t(this, req, res);
	
}

p.readValuesWithDates = function(databasePath, startDate, endDate, resolution, callback) {
	var results = [];
	var index = 0;
		
	utils.rrd.fetch(databasePath, {
		cf: "AVERAGE",
		start: startDate,
		end: endDate,
		resolution : resolution
	}, function(time, object) {
		index++;
		
		if (object != null) {
			var o = {
				timestamp: time,
				value: object.value
			}
			
			results.push(o);
		}
		
		if (!time && !object) {
			callback(results);
			return;
		}
	});
			
}

p.createRRD = function(c, node, type, callback) {

	var result = Enumerable.From(c.settings.rrd)
					.Where(function(x) {
						for (var index in x.datatype) {
							if (x.datatype[index] == type)
								return true;
						}
						return false;
					})
					.OrderBy(function(x) {
						return x.datatype;
					}).ToArray();
	
	if (result.length == 0) {
		callback(null, "No configuration for datatype found");
		return;
	}

	var object = result[0];
	var dbPath = c.storagePath() + node.toString() + "-" + type + ".rrd";

	if (!fs.existsSync(dbPath)) {
		var now = Math.ceil((new Date).getTime() / 1000);
		
		utils.rrd.create(dbPath, object.steps, now, object.value, function(err) {
			if (err) {
				callback(null, "RRD database could not be created: " + err);
				return;
			}
			else {
				callback(dbPath, null);
			}
		});			
	}
	else {
		callback(dbPath, null);
	}
}

p.listNodes = function(req, res) {

	var t = function(c, request, response) {
	
		utils.logging.info(c.identity().identifier, util.format("[%s] Listing all nodes", request.ip));
		
		var results = [];
		
		var db = utils.db.getConnection();

		var query = "SELECT rowid as nodeid, friendlyname, ipaddress, registered FROM nodes ORDER BY friendlyname, NodeID";
	    db.each(query, function(err, row) {
	    	if (err) {
		    	console.log(err);
	    	}
	        results.push(row);
	    }, function() {
		    res.statusCode = 200;			
			res.jsonp(results);
	    });
	};
	
	t(this, req, res);
}

p.removeNode = function(req, res) {

	var t = function(c, request, response) {
		
		// Remove SQL query
		var query = "DELETE FROM nodes WHERE rowid = ?";
		var connection = utils.db.getConnection();

		connection.run(query, [request.params.node], function(err, row) {
			
			if (err) {
				res.statusCode = 500;
				res.send(err);
			}
			else {
				res.statusCode = 200;
				res.send("OK");
			}
			
		});
	};
	
	
	t(this, req, res);
}

/* A node is started and detects xnodes-central for the first time */
p.registerNode = function(req, res) {

	var t = function(c, request, response) {
		var node = -1;
		if (req.params.node != null)
			node = req.params.node;
			
		utils.logging.info(c.identity().identifier, util.format("Node with IP %s is registering with xnodes-central (nodeID: %d)", request.ip, node));
			
		var connection = utils.db.getConnection();
		var nodeQuery = "SELECT rowid FROM nodes WHERE rowid = ?";
		connection.get(nodeQuery, node, function(err, row) {
			if (!err && !row) {
				var insertQuery = "INSERT INTO nodes VALUES (?, ?, ?)";
				connection.run(insertQuery, [request.ip, "", new Date().getTime()], function(err, row) {
					
					if (err) {
						utils.logging.error(c.identity().identifier, util.format("Node with IP %s could not be registered with xnodes-central (nodeID: %d): %s", request.ip, node, err));
						response.statusCode = 500;
						response.send(err);
					}
					else {
						var nodeID = this.lastID;
						utils.logging.info(c.identity().identifier, util.format("Node with IP %s is registered with xnodes-central (nodeID: %d)", request.ip, nodeID));
						
						c.invalidateNodesOnIP(request.ip, nodeID);

						response.send(200, nodeID.toString());
					}
					
				});
			}
			else if (!err && row) {
				utils.logging.info(c.identity().identifier, util.format("[%s] Updating IP for node (nodeID: %d)", request.ip, node));
				var updateQuery = "UPDATE nodes SET ipaddress = ?, registered = ? WHERE rowid = ?";
				
				connection.run(updateQuery, [request.ip, new Date().getTime(), node], function(err, row) {
					
					if (err) {
						utils.logging.error(c.identity().identifier, util.format("Node with IP %s could not be update with xnodes-central (nodeID: %d): %s", request.ip, node, err));
						response.statusCode = 500;
						response.send(err);
					}
					else {
						utils.logging.info(c.identity().identifier, util.format("Node with IP %s is updated with xnodes-central (nodeID: %d)", request.ip, node));
						
						c.invalidateNodesOnIP(request.ip, node);
						
						response.statusCode = 200;
						response.send(node);
					}
					
				});
			}
			else if (err) { 
				utils.logging.error(c.identity().identifier, util.format("Error querying database: %s", err));
				response.statusCode = 500;
				response.send(err);
			}
		});		
	};

	t(this, req, res);
}

p.invalidateNodesOnIP = function(ipAddress, exceptNode) {

	var t = function(c, ip, nodeid) {
		var connection = utils.db.getConnection();
		var nodeQuery = "UPDATE nodes SET ipaddress = ? WHERE rowid != ? AND ipaddress = ?";
		
		connection.run(nodeQuery, ["", exceptNode, ipAddress], function(err, row) {
			if (err) {
				utils.logging.error(c.identity().identify, util.format("A SQL error occured: %s", err));
			}
			else {
				utils.logging.info(c.identity().identifier, util.format("Other nodes with IP %s invalidated", ipAddress));
			}
		});
	}
	
	t(this, ipAddress, exceptNode);
}

p.typesAvailable = function(req, res) {
	
	var t = function(c, request, response) {
		if (req.params.node == null) {
			response.statusCode = 500;
			response.send("No nodeid defined");
		}
			
					
		var	node = req.params.node;
		var results = [];
		
		for (var index in c.settings.dataTypes) {
			var type = c.settings.dataTypes[index];

			var dbPath = c.storagePath() + node.toString() + "-" + type + ".rrd";
			
			if (fs.existsSync(dbPath)) {
				results.push(type);
			}	
		}
		
		response.statusCode = 200;
		response.jsonp(results);
	};
	
	t(this, req, res);
}

/*
p.start = function() {
	utils.logging.info(this.identity().identifier, "Starting ArduinoNode node");

	var scope = function(c) {
		c.serialConnection = new SerialPort("/dev/ttyACM0", {
	    	baudrate: 115200,
	    	parser: sp.parsers.readline("\n")
	    })
	    .on('error', function (err) {
		    console.log(err);
	    })
	    .on('open', function() {
			utils.logging.verbose(c.identity().identifier, "Succesfully openend port: /dev/ttyACM0");
	    
	    })
	    .on('data', function(data) {
			var o = parseArduinoString(data);
			if (o == null) {
				return;
			}
			
	    	if (o.type == "TEMP") {				
		    	var now = Math.ceil((new Date).getTime() / 1000);
			
				utils.rrd.update(c.rrdFile, "temp", [[now, o.temp.toString()].join(":")], function(err) {
		            if (err) {
		                utils.logging.error(c.identity().identifier, util.format("%s (%s)", c.rrdFile, err));
		            }
		            else {
		                utils.logging.verbose(c.identity().identifier, util.format("Received temp %s > %s", o.temp, c.rrdFile));
		            }
				});
				
				c.eventEmitter.emit("temperature-received", c, o);
			}
			else if (o.type == "RF") {
				utils.logging.verbose(c.identity().identifier, util.format("Received RF: %s, group: %s, unit: %s, status: %s, dim level: %s, duration: %s", 
					o.address,
					o.group,
					o.unit,
					o.status,
					o.dimlevel,
					o.duration));
					
				storeRFinDB(c, o);
				c.eventEmitter.emit("rf-received", c, o);
			}
		});  	    
	}
	
	scope(this);
}
*/

var storeRFinDB = function(c, rfSignal) {
	var connection = utils.db.getConnection();
	
	var found = false;
	connection.each("SELECT rowid FROM rfinfo WHERE address = ? AND isgroup = ? AND unit = ?", [rfSignal.address, rfSignal.group, rfSignal.unit], function(err, row) {
	
		if (err) {
			utils.logging.error(c.identity().identifier, util.format("Error querying RF database: %s", err));
		}
		else {
			found = true;
	
			var dbQuery = "UPDATE rfinfo SET lastreceived = ? WHERE rowid = ?";
			connection.run(dbQuery, [new Date().getTime(), row.rowid], function(err) {
				if (err) {
					utils.logging.error(c.identity().identifier, util.format("Error updating database: %s", err));
				}
				else {
					utils.logging.verbose(c.identity().identifier, "Updated RF info table");				
				}
				/* done updating */
			});
		}
	}, function() {
		if (!found) {
			connection.run("INSERT INTO rfinfo VALUES (?, ?, ?, ?, ?, ?)", [c.config.nodeID, '', rfSignal.address, rfSignal.group, rfSignal.unit, new Date().getTime()], function(err) {
				if (err) {
					utils.logging.error(c.identity().identifier, util.format("Error inserting record in database: %s", err));
				}
				else {
					utils.logging.info(c.identity().identifier, util.format("RF record not found, inserting: %s", JSON.stringify(rfSignal)));				
				}
			});
		}
	});
}

module.exports = ApiNode;

/* Private methods */

p.getKaku = function(req, res) {
	utils.logging.info(this.identity().identifier, util.format("[%s] Received list request [%s] module: %s", req.ip, this.config.nodeID, req.url))
	var results = [];

	var connection = utils.db.getConnection();
	var dbQuery = "SELECT rowid, * FROM rfinfo ORDER BY lastreceived DESC";
	
	connection.each(dbQuery, function(err, row) {
		
		results.push(row);
		
	}, function() {
		res.statusCode = 200; 
		res.jsonp(results);
	});
}

p.performKaku = function(req, res) {
	
	utils.logging.info(this.identity().identifier, util.format("[%s] Received KAKU request [%s] module: %s", req.ip, this.config.nodeID, req.url))
	// /switch/:brand/:type/:address/:number/:command
	var results = { status: "OK" };
	
	var commandString = "RF:";
	commandString += req.params.address;
	commandString += ":";
	commandString += req.params.type.toLowerCase();
	commandString += ":";
	commandString += req.params.number;
	commandString += ":";
	commandString += req.params.command.toLowerCase();	
	
	utils.logging.info(this.identity().identifier, util.format("Sending %s to Arduino", commandString));
	
	this.serialConnection.write(commandString);
	
	res.statusCode = 200; 
	res.jsonp(results);
}

p.getRequest = function(req, res) {
	utils.logging.info(this.identity().identifier, util.format("[%s] Received request [%s] module: %s", req.ip, this.config.nodeID, req.url))
	
	var resolution = 1;
	if (req.params.resolution != null) {
		resolution = req.params.resolution;
	}
	
	var records = 60;
	if (req.params.records != null) {
		records = req.params.records;
	}
	
	var endDate = Math.floor(Date.now() / 1000) - 1;
	if (req.params.enddate != null) {
		utils.logging.info(this.identity().identifier, util.format("End date is specified: %s", req.params.enddate));
		endDate = Math.floor(req.params.enddate);
		console.log(new Date(endDate));
	}
	
	var v = new utils.Validator();
	v.check(resolution).isInt();
	v.check(records).isInt();
	v.check(endDate).isInt();
	
	var errors = v.getErrors();
	if (errors.length > 0) {
		res.end(JSON.stringify(errors));
		return;
	}
	
	var timeStampBegin = endDate - (resolution * records);
	
	var results = [];
		
	var index = 0;
	

	utils.rrd.fetch(this.rrdFile, {
		cf: "AVERAGE",
		start: timeStampBegin,
		end: endDate,
		resolution : resolution
	}, function(time, object) {
		index++;
		
		if (object != null && object.temp) {
			var o = {
				timestamp: time,
				value: object.temp
			}
			
			results.push(o);
		}
		
		if (!time && !object) {
			res.statusCode = 200; 
			res.jsonp(results);
		}
	});
}


