var NodeBase = require("../Base/NodeBase");
var util = require("util");
var events = require('events');
var fs = require("fs");
var utils = require("../../utilities/utils");
var sp = require("serialport");
var SerialPort = require("serialport").SerialPort
var http = require("http");

var ArduinoNode = function() {
	NodeBase.call(this);
	this.settings = null;
	this.serialConnection = null
}

util.inherits(ArduinoNode, NodeBase);
//util.inherits(ArduinoNode, events.EventEmitter)

var p = ArduinoNode.prototype;

p.defaultSettings = function() {
	return {
	};
}

p.identity = function() {
	return {
		identifier : "org.xnodes.arduinonode", // required property
		version : "1.0", // required property
	};
}

p.initialize = function(configuration, callback) {
	this.config = configuration;

	utils.logging.info(this.identity().identifier, "Initializing Arduino node");

	this.start();
	callback();
}

p.webRoutes = function() {
	this.performKaku = utils.bind(this.performKaku, this);
	this.getKaku = utils.bind(this.getKaku, this);

	return [
		{ path: "/switch/:brand/list", method: this.getKaku },
		{ path: "/switch/:brand/:type/:address/:number/:command", method: this.performKaku },
	];
}

p.publicEvents = function() {
	return [
		"temp-received",
		"rf-received"
	];
}

p.hooksInto = function() {
	return [
	];
}

p.cleanUp = function() {
	utils.logging.info(this.identity().identifier, "Cleaning up ArduinoNode node");
}

p.start = function() {
	utils.logging.info(this.identity().identifier, "Starting ArduinoNode node");

	var scope = function(c) {
		c.serialConnection = new SerialPort("/dev/ttyACM0", {
	    	baudrate: 9600,
	    	parser: sp.parsers.readline("\n")
	    })
	    .on('error', function (err) {
		    console.log(err);
	    })
	    .on('open', function() {
			utils.logging.verbose(c.identity().identifier, "Succesfully openend port: /dev/ttyACM0");
	    
	    })
	    .on('data', function(data) {
			var o = parseArduinoString(data);
			if (o == null) {
//				console.log(data);
				return;
			}
			
//	    	var now = Math.ceil((new Date).getTime() / 1000);
	    	switch (o.type) {
		    	case "temp":
		    	case "power.t1.usage":
		    	case "power.t2.usage":
		    	case "power.t1.delivered":
		    	case "power.t2.delivered":
		    	case "power.current.usage":
		    	case "power.current.delivered":
		    	{
					c.sendValueToAPI(c, o);
		    	}
		    	default:
		    	{
			    	
		    	}
	    	}
/*
			else if (o.type == "RF") {
				
				utils.logging.verbose(c.identity().identifier, util.format("Received RF: %s, group: %s, unit: %s, status: %s, dim level: %s, duration: %s", 
					o.address,
					o.group,
					o.unit,
					o.status,
					o.dimlevel,
					o.duration));
				storeRFinDB(c, o);
				c.eventEmitter.emit("rf-received", c, o);
			}
*/
		});  	    
	}
	
	scope(this);
}

p.sendValueToAPI = function(c, o) {
	var options = {
		host: "localhost",
		port: 3000,
		path: "/nodes/store/0/" + o.type.toLowerCase() + "/" + o.value + "?apikey=abcdefghijklmop"
	};
	
	http.get(options, function(resp){
		resp.on("data", function(data) {
			//utils.logging.verbose(c.identity().identifier, "Response of storage API: " + data);	
		});
		
		resp.on("error", function(e) {
			utils.logging.error(c.identity().identifier, "Error accessing storage API: " + e);
		});
	});	
}

/*
var storeRFinDB = function(c, rfSignal) {
	var connection = utils.db.getConnection();
	
	var found = false;
	connection.each("SELECT rowid FROM rfinfo WHERE address = ? AND isgroup = ? AND unit = ?", [rfSignal.address, rfSignal.group, rfSignal.unit], function(err, row) {
	
		if (err) {
			utils.logging.error(c.identity().identifier, util.format("Error querying RF database: %s", err));
		}
		else {
			found = true;
	
			var dbQuery = "UPDATE rfinfo SET lastreceived = ? WHERE rowid = ?";
			connection.run(dbQuery, [new Date().getTime(), row.rowid], function(err) {
				if (err) {
					utils.logging.error(c.identity().identifier, util.format("Error updating database: %s", err));
				}
				else {
					utils.logging.verbose(c.identity().identifier, "Updated RF info table");				
				}
			});
		}
	}, function() {
		if (!found) {
			connection.run("INSERT INTO rfinfo VALUES (?, ?, ?, ?, ?, ?)", [c.config.nodeID, '', rfSignal.address, rfSignal.group, rfSignal.unit, new Date().getTime()], function(err) {
				if (err) {
					utils.logging.error(c.identity().identifier, util.format("Error inserting record in database: %s", err));
				}
				else {
					utils.logging.info(c.identity().identifier, util.format("RF record not found, inserting: %s", JSON.stringify(rfSignal)));				
				}
			});
		}
	});
}
*/
var parseArduinoString = function(data) {
	data = data.replace('\r', '');
	
	var parts = data.split(":");
		
	if (parts.length == 2 && parts[0] == "TEMP") {
		return {
			type: "temp",
			value: parts[1]
		};		
	}
	else if (parts.length == 2 && parts[0] == "POWER") {
		return {
			type: "power.current.usage",
			value: result[1]
		}
	}
	else if (parts.length == 2 && parts[0] == "GAS") {
		return {
			type: "GAS",
			value: parts[1]
		}
	}
	else if (parts.length == 7 && parts[0] == "RF") {
		console.log(data);
		return {
			type: "RF",
			address: parts[1],
			group: parts[2],
			unit: parts[3],
			status: parts[4],
			dimlevel: parts[5],
			duration: parts[6]
		};
	}
	else {
	
		var powerTypes = [
			{
				type: "power.t1.usage",
				regex: /1-0:1.8.1\(([0-9.]*?)\*kWh\)/
			},
			{
				type: "power.t2.usage",
				regex: /1-0:1.8.2\(([0-9.]*?)\*kWh\)/
			},
			{
				type: "power.current.usage",
				regex: /1-0:1.7.0\(([0-9.]*?)\*kW\)/
			}
		];
		
		for (var index in powerTypes) {
			var pt = powerTypes[index];
			
			if (data.match(pt.regex)) {
				var result = data.match(pt.regex);
				var value = (parseFloat(result[1]) * 1000).toFixed();
				value = utils.leftTrim(value, "0");
	
				return {
					type: pt.type,
					value: value
				};
			}
		}
		return null;
	}
	
}

module.exports = ArduinoNode;

/* Private methods */

p.getKaku = function(req, res) {
	utils.logging.info(this.identity().identifier, util.format("[%s] Received list request [%s] module: %s", req.ip, this.config.nodeID, req.url))
	var results = [];

	var connection = utils.db.getConnection();
	var dbQuery = "SELECT rowid, * FROM rfinfo ORDER BY lastreceived DESC";
	
	connection.each(dbQuery, function(err, row) {
		
		results.push(row);
		
	}, function() {
		res.statusCode = 200; 
		res.jsonp(results);
	});
}

p.performKaku = function(req, res) {
	
	utils.logging.info(this.identity().identifier, util.format("[%s] Received KAKU request [%s] module: %s", req.ip, this.config.nodeID, req.url))
	// /switch/:brand/:type/:address/:number/:command
	var results = { status: "OK" };
	
	var commandString = "RF:";
	commandString += req.params.address;
	commandString += ":";
	commandString += req.params.type.toLowerCase();
	commandString += ":";
	commandString += req.params.number;
	commandString += ":";
	commandString += req.params.command.toLowerCase();	
	
	utils.logging.info(this.identity().identifier, util.format("Sending %s to Arduino", commandString));
	
	this.serialConnection.write(commandString);
	
	res.statusCode = 200; 
	res.jsonp(results);
}
