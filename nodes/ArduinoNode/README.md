ArduinoNode
-

**Information** | Node details
-------------   | ------------- 
Identifier      | org.xnodes.arduinonode  
Version         | 1.0 
Last Update     | March 1st 2013
Requirements    | NodeBase, serialport, util, events, fs, utils


**ArduinoNode** is a node which expects certain output from your Arduino device. The Arduino device currently needs to be connected to the USB port of your Raspberry.

