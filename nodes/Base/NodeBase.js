var config = require("../../utilities/config");
var fs = require("fs");
var EventEmitter = require("events").EventEmitter;

// ---------
// Default interface for modules
// ---------

var NodeBase = function() {
	this.eventEmitter = new EventEmitter();
}

var p = NodeBase.prototype;

p.defaultSettings = function() {
	throw new Error("defaultSettings function not implemented");
}

p.identity = function() {
	throw new Error("identify function not implemented");	
}

p.initialize = function(configuration, callback) {
	throw new Error("initialize function not implemented");
}

p.webRoutes = function() {
	return [];
}

p.publicEvents = function() {
	return [];
}

p.hooksInto = function() {
	return [];
}

p.cleanUp = function() {
}

p.saveSettings = function() {
}

p.storagePath = function() {
//	throw new Error("storagePath function not implemented");

	var subdirName = this.identity().identifier + "/" + this.config.nodeID + "/";
	subdirName = config.storagePath() + subdirName;
	
	var paths = subdirName.split("/");
	
	var dir = "/";
	for (var index in paths) {
		if (paths[index].length == 0)
			continue;
		dir = dir + paths[index] + "/";

		if (!fs.existsSync(dir)) {
			fs.mkdirSync(dir);
		}
	}
	
	return subdirName;
}

module.exports = NodeBase;