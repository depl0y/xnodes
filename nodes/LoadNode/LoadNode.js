var NodeBase = require("../Base/NodeBase");
var util = require("util");
var events = require('events');
var utils = require("../../utilities/utils");
var http = require("http");
var os = require('os');

var LoadNode = function() {
	NodeBase.call(this);
	this.settings = null;
	this.serialConnection = null
}

util.inherits(LoadNode, NodeBase);
//util.inherits(ArduinoNode, events.EventEmitter)

var p = LoadNode.prototype;

p.defaultSettings = function() {
	return {
	};
}

p.identity = function() {
	return {
		identifier : "org.xnodes.loadnode", // required property
		version : "1.0", // required property
	};
}

p.initialize = function(configuration, callback) {
	this.config = configuration;

	utils.logging.info(this.identity().identifier, "Initializing LoadNode");

	this.start();
	callback();
}

p.publicEvents = function() {
	return [
		"temp-received",
		"rf-received"
	];
}

p.hooksInto = function() {
	return [
	];
}

p.cleanUp = function() {
	utils.logging.info(this.identity().identifier, "Cleaning up LoadNode node");
}

p.start = function() {
	utils.logging.info(this.identity().identifier, "Starting LoadNode node");

	var t = function(c) {
		setInterval(function() {
			c.getLoad(c);
		}, 2000);
	}
	
	t(this);
}

p.getLoad = function(c) {

	var loads = os.loadavg();
		
	var o = {
		type: "cpuload",
		value: (loads[0] * 100).toFixed(2)
	};
		
	c.sendValueToAPI(c, o);

}

p.sendValueToAPI = function(c, o) {
	var options = {
		host: "localhost",
		port: 3000,
		path: "/nodes/store/0/" + o.type.toLowerCase() + "/" + o.value + "?apikey=abcdefghijklmop"
	};
	
	http.get(options, function(resp){
		resp.on("data", function(data) {
			//utils.logging.verbose(c.identity().identifier, "Response of storage API: " + data);	
		});
		
		resp.on("error", function(e) {
			utils.logging.error(c.identity().identifier, "Error accessing storage API: " + e);
		});
	});	
}

module.exports = LoadNode;