var NodeBase = require("../Base/NodeBase");
var util = require("util");
var fs = require("fs");
var utils = require("../../utilities/utils");


var LoggingNode = function() {
	NodeBase.call(this);
	this.settings = null;

}

util.inherits(LoggingNode, NodeBase);

var p = LoggingNode.prototype;

p.defaultSettings = function() {
	return {
		pageSize : {
			type: "integer",
			value: 10
		}
	};
}

p.identity = function() {
	return {
		identifier : "org.xnodes.logging", // required property
		version : "1.0", // required property
	};
}

p.initialize = function(configuration, callback) {
	this.config = configuration;
	utils.logging.info(this.identity().identifier, "Initializing LoggingNode node");

	callback();
}

p.webRoutes = function() {

	this.getRequest = utils.bind(this.getRequest, this);

	return [
		{ path: "/logging", method: this.getRequest },
		{ path: "/logging/:minlevel", method: this.getRequest },
		{ path: "/logging/:minlevel/:page", method: this.getRequest },
		{ path: "/logging/:minlevel/:page/:pagesize", method: this.getRequest }
	];
}

p.hooksInto = function() {
	return [];
}

p.cleanUp = function() {
	utils.logging.info(this.identity().identifier, "Cleaning up LoggingNode node");
}

module.exports = LoggingNode;

/* Private methods */

p.getRequest = function(req, res) {
	utils.logging.info(this.identity().identifier, util.format("Received request for LoggingNode [%d] module: %s", this.config.nodeID, req.url));
	
	var callback = req.param("jsoncallback");

	var minlevel = 0;
	if (req.params.minlevel != null)
		minlevel = req.params.minlevel;
		
	var page = 1;
	if (req.params.page != null) {
		page = req.params.page;
	}
	
	var pagesize = 20;
	if (req.params.pagesize != null) {
		pagesize = req.params.pagesize;
	}
	
	var v = new utils.Validator();
	v.check(minlevel).isInt();
	v.check(page).isInt();
	v.check(pagesize).isInt();
	
	var errors = v.getErrors();
	if (errors.length > 0) {
		res.end(JSON.stringify(errors));
		return;
	}
	
	
	var startIndex = (page - 1) * pagesize;
	var additionToQuery = "";
	if (startIndex != 0)
		additionToQuery = ", " + startIndex;
	
	var results = [];
	
	var db = utils.db.getConnection();
	
	var query = util.format("SELECT * FROM logging WHERE logtype >= %d ORDER BY DATETIME DESC LIMIT %d %s", minlevel, pagesize, addtionToQuery);
    db.each(query, function(err, row) {
    	if (err) {
	    	console.log(err);
    	}
        results.push(row);
    }, function() {
	    
	    res.statusCode = 200;			
		res.jsonp(results);
    });
    

}