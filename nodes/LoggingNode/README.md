LoggingNode
-

**Information** | Node details
-------------   | ------------- 
Identifier      | org.xnodes.logging  
Version         | 1.0 
Last Update     | March 1st 2013
Requirements    | NodeBase, util, fs, utils


**LoggingNode** is a node which creates an API end-point which can be used to export log rows.

