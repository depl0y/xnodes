var NodeBase = require("../Base/NodeBase");
var util = require("util");
var fs = require("fs");
var utils = require("../../utilities/utils");


var RandomNode = function() {
	NodeBase.call(this);
	this.settings = null;

}

util.inherits(RandomNode, NodeBase);

var p = RandomNode.prototype;

p.defaultSettings = function() {
	return {
		interval : {
			type: "integer",
			value: 10
		},
		maxValue: {
			type: "integer",
			value: 200
		},
		minValue: {
			type: "integer",
			value: 0
		}
	};
}

p.identity = function() {
	return {
		identifier : "org.xnodes.random", // required property
		version : "1.0", // required property
	};
}

p.initialize = function(configuration, callback) {
	this.config = configuration;

	utils.logging.info(this.identity().identifier, "Initializing RandomNode node");
	this.rrdFile = this.storagePath() + this.config.nodeID.toString() + ".rrd";


	if (!fs.existsSync(this.rrdFile)) {
		utils.logging.info(this.identity().identifier, "RRD database does not exist, creating: " + this.rrdFile);
		
		var now = Math.ceil((new Date).getTime() / 1000);
		
		var t = function(c) {
			var rrdSettings = [
				"DS:random:GAUGE:600:U:U",
				"RRA:AVERAGE:0.0:1:3600",
				"RRA:AVERAGE:0.0:60:1440",
				"RRA:AVERAGE:0.0:900:2880",
				"RRA:AVERAGE:0.0:3600:9480",
			];

			utils.rrd.create(c.rrdFile, 1, now, rrdSettings, function(err) {
				if (err)
					utils.logging.error(c.identity().identifier, "RRD database could not be created...");
				else {
					utils.logging.info(c.identity().identifier, "RRD database created...");
					c.start();
				}
			});
		};
		
		t(this);
	}
	else
		this.start();

	callback();
}

p.webRoutes = function() {

	this.getRequest = utils.bind(this.getRequest, this);

	return [
		{ path: "/random", method: this.getRequest },
		{ path: "/random/:records", method: this.getRequest },
		{ path: "/random/:resolution/:records", method: this.getRequest }
	];
}

p.hooksInto = function() {
	return [
		{ 
			node : "org.xnodes.arduinonode",
			nodeID: 5,
			method: this.rfReceived,
			eventName: "rf-received"
		}
	];
}

p.rfReceived = function(nodeId, object) {
	utils.logging.info(this.identity().identifier, "Received RF from node: " + nodeId);
}

p.cleanUp = function() {
	utils.logging.info(this.identity().identifier, "Cleaning up RandomNode node");
}

p.start = function() {
	utils.logging.info(this.identity().identifier, "Starting RandomNode node");

	this.doRandomNumber = utils.bind(this.doRandomNumber, this);	
	
	this.doRandomNumber(this);
}

module.exports = RandomNode;

/* Private methods */

p.temperatureReceived = function(data) {
	console.log("Temp: " + data);
}

p.getRequest = function(req, res) {
	utils.logging.info(this.identity().identifier, "Received request for Random [" + this.config.nodeID + "] module: " + req.url);
	
	var resolution = 1;
	if (req.params.resolution != null) {
		resolution = req.params.resolution;
	}
	
	var records = 60;
	if (req.params.records != null) {
		records = req.params.records;
	}
	utils.logging.verbose(this.identity().identifier, "Request params " + resolution + " / " + records);
	
	var timeStamp = Math.floor(Date.now() / 1000) - 1;
	var timeStampBegin = timeStamp - (resolution * records);
	
	var results = [];
		
	var index = 0;

	utils.rrd.fetch(this.rrdFile, {
		cf: "AVERAGE",
		start: timeStampBegin,
		end: timeStamp,
		resolution : resolution
	}, function(time, object) {
		index++;
		
		if (object != null && object.random != null) {
//			console.log(index + " - " + time + " - " + object.random);
			
			var o = {
				timestamp: time,
				value: object.random
			}
			
			results.push(o);
		}
		
		if (!time && !object) {
			res.send(results);
		}
	});
}

p.doRandomNumber = function(parent) {
	
	var t = function(o) {
		setInterval(function() {
			var randomNumber = Math.floor(Math.random() * 100);
			var now = Math.ceil((new Date).getTime() / 1000);
			
			utils.rrd.update(o.rrdFile, "random", [[now, randomNumber.toString()].join(":")], function(err) {
	            if (err) {
	                utils.logging.error(o.identity().identifier, parent.rrdFile + " (" + err + ")");
	                utils.logging.error(o.identity().identifier, "[" + now.toString() + "] [" + randomNumber + "]");
	            }
	            else {
	                utils.logging.verbose(o.identity().identifier, "Generated " + randomNumber + " > " + parent.rrdFile);
	            }
			});
			/*
	
			utils.hoard.update(parent.hoardFile, randomNumber, timeStamp, function(err) {
	        });
	        */
		}, 2500);
	};
	
	t(this);
}


