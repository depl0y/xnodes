var Path = require('path');
var d = require("./db");
var Q = require('q');

exports.databasePath = function() {
    return Path.dirname(require.main.filename) + "/storage/xnodes.db";
};

exports.storagePath = function() {
    return Path.dirname(require.main.filename) + "/storage/";
};

exports.nodesPath = function() {
    return Path.dirname(require.main.filename) + "/nodes/";
};


exports.databaseImportFile = function() {
    return Path.dirname(require.main.filename) + "/database/db.sql";    
}

exports.getNodes = function(callback) {
    var results = [];
    
    var db = d.getConnection();
    db.each("SELECT module, key, value, ROWID as nodeID FROM config WHERE module = 'nodes' AND key = 'node'", function(err, row) {
        if (row.value.indexOf("{") > -1) {
            row.value = JSON.parse(row.value);
        }
        results.push(row);
    }, function() {
	    callback(results);
    });
    
}

exports.getSectionValues = function(module) {
    
    var response = Q.defer();
    var results = [];
    
    var db = d.getConnection();
    db.each("SELECT module, key, value FROM config WHERE module = ?", module, function(err, row) {
        if (row.value.indexOf("{") > -1) {
            row.value = JSON.parse(row.value);
        }
        
        results.push(row);
    }, function() {
        response.resolve(results);
    });
    
    
    return response.promise;
}

exports.getValue = function(module, key) {
    var response = Q.defer();
    
    var db = d.getConnection();
    db.run("SELECT module, key, value FROM config WHERE module = ? AND key = ?", [ module, key], function(err, row) {
        console.log(row);
    }, function() {
        response.resolve(results);
    });
    
    
    return response.promise;
}
