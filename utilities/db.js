var c = require('./config');
var sqlite = require('sqlite3');

var db = function db(){

    var sqlConnection = null;
	
	this.getConnection = function() {
	   if (sqlConnection == null)
    	   sqlConnection = new sqlite.Database(c.databasePath());
	   return sqlConnection;
	}
	
	if(db.caller != db.getInstance){
		throw new Error("This object cannot be instanciated");
	}
}


db.instance = null;

db.getInstance = function(){
	if(this.instance === null){
		this.instance = new db();
	}
	return this.instance;
}

module.exports = db.getInstance();