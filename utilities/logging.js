var colors = require('colors');
var d = require("./db");
var dateFormat = require('dateformat');

log = function (module, message, logtype, toDatabase) {
    

    var now = new Date();
    var dateString = dateFormat(now, "HH:MM:ss:l");
    var str = dateString + " : " + logtype.toString() + " : " + module + " : " + message;
    
    if (logtype == 0) {
	  	toDatabase = false;
        str = str.grey;
    }
    else if (logtype == 1) {
	  	toDatabase = false;
        str = str.white;
    }
    else if (logtype == 5) {
        str = str.red.bold;
    }
    
    if (logtype >= global.loglevel)
        console.log(str);
        
    if (toDatabase !== false) {
	    try {
		    var db = d.getConnection();
		    db.run("INSERT INTO logging VALUES (?, ?, ?, ?);", [new Date().getTime(), logtype, module, message]);
	    }
	    catch (err) {
		    log("logging", "Failed to log to database: " + err, 5, false);
	    }
    }
};

exports.verbose = function(module, message) {
    log(module, message, 0);
};

exports.info = function(module, message) {
    log(module, message, 1);
};

exports.warn = function(module, message) {
    log(module, message, 3);
};

exports.error = function(module, message) {
    log(module, message, 5);
};