var path = require("path");
var fs = require("fs");
//var log = require('./logging')

exports.config = require("./config");
exports.logging = require("./logging");
exports.db = require("./db");
exports.rrd = require("../lib/rrd");
exports.Validator = require("validator").Validator;

exports.bind = function(fn, scope) {
  return function () {
    return fn.apply(scope, arguments);
  }
};

exports.now = function() {
	return Math.ceil((new Date).getTime() / 1000);	
}

exports.Validator.prototype.error = function (msg) {
    this._errors.push(msg);
    return this;
}

exports.Validator.prototype.getErrors = function () {
    return this._errors;
}

if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}

String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}

exports.leftTrim = function(str, charToTrim) {

	if (str.length == 0)
		return "";

	while(str.startsWith("0") && str.length > 0) {
		str = str.substr(1);
	}
	
	return str;
}

