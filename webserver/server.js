var logging = require("../utilities/logging");
var express = require('express');
var mdns = require('mdns');
var Q = require('q');
var utils = require("../utilities/utils");
var util = require('util');

var Server = function(configObject) {
  this.configObject = configObject;
  logging.info(this.identifier, "Initializing webserver");
}

// properties and methods

Server.prototype = {
	configObject: null,
	identifier: "webserver",
	serverObject: null,
	advertisementObject: null,
	apiKey : "abcdefghijklmop"
}

Server.prototype.start = function(completed, failed) {

	var httpPort = 3000;

    this.serverObject = express();
    this.serverObject.enable("jsonp callback");    
    this.serverObject.engine('html', require('ejs').renderFile);
    this.serverObject.engine('htm', require('ejs').renderFile);
    this.serverObject.set('jsonp callback name', 'jsoncallback');

    this.serverObject.use("/web", express.static("/root/xnodes/xnodes-web"));

    this.serverObject.listen(httpPort);
    this.serverObject.get('/graph', function (req, res)
    {
    	res.render('../xnodes-web/redirect.htm');
    });
    
    var txt_record = {
	};
    

    logging.info(this.identifier, "Initializing Bonjour service (http :: xnodes)");
	this.advertisementObject = mdns.createAdvertisement(mdns.tcp('http'), httpPort, {name: "xnodes"});
	this.advertisementObject.start();
	
	this.requireAuthentication = utils.bind(this.requireAuthentication, this);

    
    completed();
}

Server.prototype.requireAuthentication = function(req, res, next) {
//	res.send("You are not authorized to see this page");	
	if (req.query.apikey != this.apiKey) {
		logging.error(this.identifier, util.format("[%s] Request to API with no or wrong API key", req.ip));
		res.send(403, "You should supply the right API key with this request");
		return;
	}

	next();
}

Server.prototype.addHandler = function(identifier, method, path) {
	this.serverObject.get(path, this.requireAuthentication, method);
	logging.verbose(this.identifier, "Handler added on " + path + " for " + identifier);
}

module.exports = Server;