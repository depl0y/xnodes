var cpuGauge = null;
var tempGauge = null;
var usageGauge = null;
var costGauge = null;

var addResult = function(data, series) {

	for (var index in data) {
		var object = data[index];
		
		if (object.value == null)
			continue;

		var found = false;

		object.timestamp = object.timestamp * 1000;
		
		var xDataLength = chart.series[series].xData.length;
		
		var lastValue = 0;
		if (xDataLength > 0)
			lastValue = chart.series[series].xData[xDataLength - 1];
			
		if (object.timestamp > lastValue && object.value) {
			var shouldShift = (xDataLength > 60);
			chart.series[series].addPoint([object.timestamp, object.value], false, shouldShift);
		}
	}
	
	chart.redraw();
};

var getPoint = function(series, startDate, endDate, resolution) {
	var startDateString = (startDate.getTime() / 1000).toFixed(0).toString();
	
	var endDateString = ""; //
	if (endDate != "0") {
		endDateString = (endDate.getTime() / 1000).toFixed(0).toString();
	}
	else {
		endDateString = "0"						
	}
	
	$.getJSON("/nodes/read/0/temp/" + resolution + "/" + startDateString + "/" + endDateString +"?apikey=abcdefghijklmop&jsoncallback=?", {},
	function(data) {
		addResult(data, 0);
	});	

	$.getJSON("/nodes/read/0/power.current.usage/" + resolution + "/" + startDateString + "/" + endDateString + "?apikey=abcdefghijklmop", {},
	function(data) {
		addResult(data, 1);
	});
};

var getCPU = function() {
	$.getJSON("/nodes/read/0/cpuload/1/last?apikey=abcdefghijklmop&jsoncallback=?", {},
	function(data) {
		cpuGauge.refresh(data.value.toFixed());
	});	
};

var getLastTemp = function() {
	$.getJSON("/nodes/read/0/temp/1/last?apikey=abcdefghijklmop&jsoncallback=?", {},
	function(data) {
		tempGauge.refresh(data.value.toFixed(2));
	});						
}

var getLastUsage = function() {
	$.getJSON("/nodes/read/0/power.current.usage/1/last?apikey=abcdefghijklmop&jsoncallback=?", {},
	function(data) {
		usageGauge.refresh(data.value.toFixed());
	});						
}	

$(document).ready(function() {

	cpuGauge = new JustGage({
		id: "cpu-gauge", 
		value: 0, 
		min: 0,
		max: 200,
		showInnerShadow: false,
		gaugeWidthScale: 0.4,
		shadowOpacity: 0,
		shadowSize: 0,
		startAnimationType: "bounce",
		refreshAnimationType: "bounce",
		title: "CPU load"
	}); 
	
	tempGauge = new JustGage({
		id: "temp-gauge", 
		value: 0, 
		min: 0,
		max: 30,
		showInnerShadow: false,
		gaugeWidthScale: 0.4,
		shadowOpacity: 0,
		startAnimationType: "bounce",
		refreshAnimationType: "bounce",
		title: "Temperature"
	}); 
	
	usageGauge = new JustGage({
		id: "usage-gauge", 
		value: 0, 
		min: 0,
		max: 8000,
		showInnerShadow: false,
		gaugeWidthScale: 0.4,
		shadowOpacity: 0,
		startAnimationType: "bounce",
		refreshAnimationType: "bounce",
		title: "Power usage"
	}); 
	
	costGauge = new JustGage({
		id: "cost-gauge", 
		value: 0, 
		min: 0,
		max: 3,
		showInnerShadow: false,
		gaugeWidthScale: 0.4,
		shadowOpacity: 0,
		startAnimationType: "bounce",
		refreshAnimationType: "bounce",
		title: "Cost €"
	}); 	
	
  	getCPU();
    setInterval(function() {
        getCPU();
    }, 5000);

    getLastTemp();
    setInterval(function() {
    	getLastTemp();
    }, 5000);
    
    getLastUsage();
    setInterval(function() {
    	getLastUsage();
    }, 5000);
	
});